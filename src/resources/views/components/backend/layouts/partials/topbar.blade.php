<header class="topbar" data-navbarbg="skin5">
    <nav class="navbar top-navbar navbar-expand-md navbar-dark">
        <div class="navbar-header" data-logobg="skin6">
            <a class="navbar-brand" href="{{ route('dashboard') }}">
                <b class="logo-icon">
                    <img src="{{ asset('vendor/plugins/images/logo.png') }}" alt="Logo" class="brand-image img-circle elevation-3" style="opacity: .8; height:50px; width:50px">
                </b>
                <span class="logo-text">
                    <img src="{{ asset('vendor/plugins/images/sun.png') }}" alt="homepage" style="width: 60px"/>
                </span>
            </a>
            <a class="nav-toggler waves-effect waves-light text-dark d-block d-md-none"
                href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
        </div>
        <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
            <ul class="navbar-nav ms-auto d-flex align-items-center">
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="profile.html"
                        aria-expanded="false">
                        <i class="fa fa-user text-white" aria-hidden="true"></i>
                        <span class="text-white font-medium">Profile</span></a>
                    </a>
                </li>
                <li>
                    <a class="profile-pic" href="#">
                        <img src="{{ asset('vendor/plugins/images/users/profile.png') }}" alt="user-img" width="36"
                            class="img-circle"><span class="text-white font-medium">User Name</span></a>
                </li>
            </ul>
        </div>
    </nav>
</header>